using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DisolveScript : MonoBehaviour {

    [field: Header("Autoattach properties")]
    [field: SerializeField, GetComponent, ReadOnlyField] private MeshRenderer meshRenderer { get; set; }
    //[field: SerializeField] private bool revalidateProperties { get; set; }

    [field: Header("Disolve properties")]
    [field: SerializeField, ReadOnlyField] private float dissolveAmount { get; set; } = 0f;
    [field: SerializeField] private bool useBelowProperties { get; set; }
    [field: SerializeField] private Color edgeColor { get; set; }
    [SerializeField] private Vector3 direction;
    [field: SerializeField] private bool useNoise;

    [field: Header("Alternative functionality")]
    [field: SerializeField] private bool useReplaceMaterial { get; set; }
    [field: SerializeField] private Material replaceMaterial { get; set;}

    [field: Header("Debug")]
    [field: SerializeField, ReadOnlyField] private Material dissolveMat;    // Si buscamos aqui se aplica a todos los objetos con ese material

    public bool dissolved { get; private set; }

    /*    void OnValidate() {
    #if UNITY_EDITOR
            UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
            bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
            bool prefabConnected = PrefabUtility.GetPrefabInstanceStatus(this.gameObject) == PrefabInstanceStatus.Connected;
            if (!isValidPrefabStage && prefabConnected) {
                // Variables that will only be checked when they are in a scene
                if (!Application.isPlaying) {
                    if (revalidateProperties)
                        ValidateAssings();
                }
            }
    #endif
        }

        void ValidateAssings() {
            if (dissolveMat == null || revalidateProperties) {
                dissolveMat = rend.material;
            }

            revalidateProperties = false;
        }*/

    void Start() {

        if (!useReplaceMaterial)
            dissolveMat = meshRenderer.material;

        if (useBelowProperties) {
            dissolveMat.SetColor("_EdgeColor", edgeColor);
            dissolveMat.SetVector("_Director", direction);
            //En shader los booleanos son ints que se representan con 0 false y 1 true
            dissolveMat.SetFloat("_useNoise", useNoise == true ? 1 : 0);
        }
    }

    public void SetMaterial() {
        if (useReplaceMaterial) {
            meshRenderer.sharedMaterial = replaceMaterial;
            dissolveMat = meshRenderer.material;
        }
    }

    public void Dissolve() {
        SetMaterial();
        StartCoroutine(DissolveCo());

        /*if (gameObject.CompareTag("Tower")) {
            Debug.Log($"Piece {gameObject.name} has been called");
        }*/
    }

    IEnumerator DissolveCo() {
        while (dissolveAmount < 1) {
            dissolveAmount += Time.deltaTime;
            dissolveMat.SetFloat("_DissolverAmount", dissolveAmount);
            yield return null;
        }
        dissolveAmount = 1;
        dissolveMat.SetFloat("_DissolverAmount", dissolveAmount);
        dissolved = true;
    }

    public void Condense() {
        StartCoroutine(CondenseCo());
    }

    IEnumerator CondenseCo() {
        while (dissolveAmount > 0) {
            dissolveAmount -= Time.deltaTime;
            dissolveMat.SetFloat("_DissolverAmount", dissolveAmount);
            yield return null;
        }
        dissolveAmount = 0;
        dissolveMat.SetFloat("_DissolverAmount", dissolveAmount);
        dissolved = false;
    }
}
