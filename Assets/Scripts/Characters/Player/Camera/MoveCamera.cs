using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour {

    [field: Header("Autoattach propierties")]

    [field: Header("Camera settings")]
    [field: SerializeField] private Transform cameraPosition { get; set; }

    private bool cameraReseted { get; set; }

    void Update() {
        transform.position = cameraPosition.position;
    }

    public void ResetCameraPosition(Transform cameraPosition) {
        cameraPosition.rotation = Quaternion.Euler(0f, 0f, 0f);
        cameraPosition.localRotation = Quaternion.Euler(0f, 0f, 0f);
        /*if (!cameraReseted) {
            StartCoroutine(FixShitCamCo(cameraPosition));
        }*/
    }

    /*IEnumerator FixShitCamCo(Transform cameraPosition) {
        yield return new WaitForSeconds(1f);

        cameraPosition.rotation = Quaternion.Euler(0f, 0f, 0f);
        cameraReseted = true;
    }*/
}
