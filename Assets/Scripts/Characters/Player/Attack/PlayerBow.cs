using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBow : Bow {

    [field: Header("Autoattach properties")]
    [field: SerializeField, GetComponent, ReadOnlyField] private PlayerManager playerManager { get; set; }

    [field: Header("Player Shoot settings")]
    /*[field: SerializeField] private Rigidbody arrowPrefab { get; set; }
    [field: SerializeField] private Transform arrowSpawnPoint { get; set; }
    [field: SerializeField] private float maxShootForce { get; set; } = 20f;*/
    [field: SerializeField] private float fireRate { get; set; } = 0.5f;
    private float nextFire { get; set; }
    [field: SerializeField, Range(0, 0.1f)] private float offsetY { get; set; }
    [field: SerializeField] private Transform playerCamera { get; set; }

    /*private Rigidbody currentArrow { get; set;}
    private bool isDrawing { get; set; }*/
    private float drawStartTime { get; set; }

    void Update() {
        if (!LevelManager.IsLevelOnGoing || PauseManager.onPause)
            return;

        if (Input.GetButtonDown("Fire1") && Time.time > nextFire) {
            nextFire = Time.time + fireRate;
            StartDrawingBow();
        }
        else if (isDrawing && Input.GetButtonUp("Fire1")) {
            ReleaseArrow();
        }
    }

    void LateUpdate() {
        if (isDrawing) {
            if (currentArrow == null)
                return;
            currentArrow.transform.rotation = Quaternion.LookRotation(playerCamera.transform.forward);
        }
    }

    protected override void StartDrawingBow() {
        currentArrow = Instantiate(arrowPrefab, arrowSpawnPoint.position, playerCamera.transform.rotation);
        currentArrow.transform.parent = arrowSpawnPoint;
        drawStartTime = Time.time;
        isDrawing = true;
    }

    void ReleaseArrow() {
        if (currentArrow == null)
            return;

        float drawDuration = Time.time - drawStartTime;
        float actualShootForce = Mathf.Clamp(maxShootForce * drawDuration, 0, maxShootForce);
        currentArrow.transform.parent = null;
        //Rigidbody arrowRigidbody = currentArrow.GetComponent<Rigidbody>();
        Rigidbody arrowRigidbody = currentArrow.rb;
        arrowRigidbody.isKinematic = false;
        //arrowRigidbody.AddForce(arrowSpawnPoint.forward * actualShootForce, ForceMode.Impulse);

        Vector3 diff = playerCamera.transform.forward;

        // Choose the direction of the arrow based on hit point (only detect enemies) or use offset if there is no hit point
        if (playerManager.hit.point != Vector3.zero && !playerManager.hit.collider.CompareTag("EnemyBase")) {
            //diff = playerManager.hit.point - arrowSpawnPoint.position;
            //diff.y += 0.08f; // Add offset on Y axis to make the arrow go a little bit higher

            // Calculate the distance to the target for the arrow can reach the target. Arrow is using gravity.


            /// IA test:
            /*diff = playerManager.hit.point - arrowSpawnPoint.position;
            float distanceToTarget = diff.magnitude;

            // Calculate the velocity needed to throw the object to the target.
            float velocity = Mathf.Sqrt(distanceToTarget * Physics.gravity.magnitude);

            // Create the full initial velocity vector.
            Vector3 initialVelocity = diff.normalized * velocity;

            // Fire the arrow with the calculated initial velocity.
            arrowRigidbody.velocity = initialVelocity;*/

            // Test 2:
            diff = playerManager.hit.point - arrowSpawnPoint.position;
            float distanceToTarget = diff.magnitude;

            // Calculate the velocity needed to throw the object to the target.
            float gravity = Physics.gravity.magnitude;
            float velocity = Mathf.Sqrt((2 * gravity * distanceToTarget));

            // Create the full initial velocity vector.
            Vector3 initialVelocity = diff.normalized * velocity;

            // Fire the arrow with the calculated initial velocity.
            arrowRigidbody.velocity = initialVelocity;

        }
        else {
            // Add offset on Y axis to make the arrow go a little bit higher
            diff.y += offsetY;
        }

        arrowRigidbody.AddForce(diff.normalized * actualShootForce, ForceMode.Impulse);
        currentArrow.PlayFlySound();
        //arrowRigidbody.AddTorque(playerCamera.transform.forward * actualShootForce, ForceMode.Impulse);
        arrowRigidbody.useGravity = true;
        isDrawing = false;
        Destroy(currentArrow.gameObject, 5f);
    }
}

