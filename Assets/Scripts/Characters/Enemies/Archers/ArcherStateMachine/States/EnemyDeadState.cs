﻿using UnityEngine.AI;

internal class EnemyDeadState : EnemyArcherState {

    private int defaultLayer = 8;
    private bool alreadyDisabled { get; set; }

    public EnemyDeadState(EnemyArcher enemy, NavMeshAgent agent) : base(enemy, agent) {
        currentState = STATE.Dead;
    }

    public override void Enter() {
        isAlreadyDead = true;
        alreadyDisabled = false;

        defaultLayer = npc.layer;
        npc.layer = 12;     // Disabled layer

        agent.isStopped = true;
        agent.ResetPath();

        enemy.DisolveScript.Dissolve();
        base.Enter();
    }

    public override void Update() {
        base.Update();

        if (enemy.DisolveScript.dissolved && !alreadyDisabled) {
            alreadyDisabled = true;
            DeleteArrowsOnBody(3);
            npc.SetActive(false);
        }
    }

    public override void Exit() {

        enemy.DisolveScript.Condense();
        npc.layer = defaultLayer;
        isAlreadyDead = false;

        base.Exit();
    }
}