﻿using UnityEngine;
using UnityEngine.AI;

internal class EnemyMovingForwardState : EnemyArcherState {
    public EnemyMovingForwardState(EnemyArcher enemy, NavMeshAgent agent) : base(enemy, agent) {
        currentState = STATE.MovingForward;

        agent.speed = enemy.MoveSpeed;
        agent.isStopped = false;
        agent.autoBraking = false;
    }

    public override void Enter() {
        base.Enter();
    }

    public override void Update() {
        base.Update();

        CheckersOnUpdate();

        if (agent.remainingDistance <= agent.stoppingDistance)
            agent.SetDestination(enemy.enemies.AttackersBase.position);
    }

    public override void Exit() { 
        agent.ResetPath();

        base.Exit();
    }

    void CheckersOnUpdate() {
        CheckAllyDetected();
    }
}