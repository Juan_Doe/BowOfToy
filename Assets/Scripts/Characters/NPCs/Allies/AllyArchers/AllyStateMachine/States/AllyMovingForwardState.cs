﻿using UnityEngine.AI;

internal class AllyMovingForwardState : AllyArcherState {
    public AllyMovingForwardState(AllyArcher ally, NavMeshAgent agent) : base(ally, agent) {
        currentState = STATE.MovingForward;

        agent.speed = ally.MoveSpeed;
        agent.isStopped = false;
        agent.autoBraking = false;
    }

    public override void Update() {
        base.Update();

        CheckEnemyDetected();

        if (agent.remainingDistance <= agent.stoppingDistance)
            agent.SetDestination(ally.AlliesManager.AttackersBase.position);
    }

    public override void Exit() {
        agent.ResetPath();

        base.Exit();
    }
}