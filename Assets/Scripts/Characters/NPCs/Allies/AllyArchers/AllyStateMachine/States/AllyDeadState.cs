﻿using UnityEngine.AI;

internal class AllyDeadState : AllyArcherState {

    private int defaultLayer = 9;
    private bool alreadyDisabled { get; set; }

    public AllyDeadState(AllyArcher ally, NavMeshAgent agent) : base(ally, agent) {
        currentState = STATE.Dead;
    }

    public override void Enter() {
        isAlreadyDead = true;
        alreadyDisabled = false;

        defaultLayer = npc.layer;
        npc.layer = 12;     // Disabled layer

        agent.isStopped = true;
        agent.ResetPath();
        
        ally.DisolveScript.Dissolve();
        base.Enter();
    }

    public override void Update() {
        base.Update();

        if (ally.DisolveScript.dissolved && !alreadyDisabled) {
            alreadyDisabled = true;
            DeleteArrowsOnBody(2);
            npc.SetActive(false);
        }
    }

    public override void Exit() {

        ally.DisolveScript.Condense();
        npc.layer = defaultLayer;
        isAlreadyDead = false;

        base.Exit();
    }
}