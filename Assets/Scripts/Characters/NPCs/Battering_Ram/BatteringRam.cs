using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class BatteringRam : MonoBehaviour {

    public enum STATE { 
        Stopped,    // The battering ram is stopped waiting for conditions to move.
        Moving,     // The battering ram is moving to the enemy base.
        Breaking,   // The battering ram is breaking the enemy base's door.
        Cooldown,   // The battering ram is disabled after hit the enemy base's door three times or after losing all its health.
        Destroyed   // The battering ram is destroyed after breaking the enemy base's door.
    }

    [field: Header("Autoattach On Editor Properties")]
    [field: SerializeField, GetComponent, ReadOnlyField] private NavMeshAgent agent { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private AudioSource audioSource { get; set; }

    [field: Header("BatteringRam")]
    [field: SerializeField] private bool isEnemyRam { get; set; }
    [field: SerializeField, ReadOnlyField] private float health { get; set; } = 100f;
    [field: SerializeField] private float maxHealth { get; set; } = 100f;
    [field: SerializeField] private float timeBeforeStartSpawn { get; set; } = 30f;
    [field: SerializeField] private int hitsBeforeCooldown { get; set; } = 2;
    private int hitsCounter { get; set; }
    [field: SerializeField] private float timeBetweenHits { get; set; } = 10f;
    private float timeCounter { get; set; }
    [field: SerializeField] private float stepBackDistance { get; set; } = 2f;
    [field: SerializeField] private float cooldownTime { get; set; } = 10f;
    [field: SerializeField] private int inactiveLayerIndex { get; set; } = 12;
    private int originalLayer { get; set; }

    [field: Header("Waypoints")]
    [field: SerializeField] private Transform[] waypoints { get; set; }
    [field: SerializeField, ReadOnlyField] private int currentWaypoint { get; set; } = 0;

    [field: Header("UI")]
    [field: SerializeField] private Image healthBar { get; set; }
    [field: SerializeField, ReadOnlyField] private Text healthText { get; set; }
    [field: SerializeField, ReadOnlyField] private GameObject healthGroupGO { get; set; }
    [field: SerializeField] private bool revalidateProperties { get; set; }

    [field: Header("FX")]
    [field: SerializeField] private AudioSource destroySound { get; set; }
    [field: SerializeField] private ParticleSystem destroyFX { get; set; }

    [field: Header("Debug")]
    [field: SerializeField, ReadOnlyField] private STATE currenState { get; set; } = STATE.Stopped;
    [field: SerializeField, ReadOnlyField] private bool isDead { get; set; }

    private Coroutine cooldownCo { get; set; }
    private Vector3 spawnPosition;
    private bool onStepBack { get; set; }
    private int permanentHitsCounter { get; set; }

    private bool onDestination { get; set; }

    void OnValidate() {
#if UNITY_EDITOR
        UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
        bool prefabConnected = PrefabUtility.GetPrefabInstanceStatus(this.gameObject) == PrefabInstanceStatus.Connected;
        if (!isValidPrefabStage && prefabConnected) {
            // Variables that will only be checked when they are in a scene
            if (!Application.isPlaying) {
                if (revalidateProperties)
                    ValidateAssings();
            }
        }
#endif
    }

    void ValidateAssings() {
        if (healthText == null || revalidateProperties) {
            healthText = healthBar.transform.GetChild(0).GetComponent<Text>();
        }

        if (healthGroupGO == null || revalidateProperties) {
            healthGroupGO = healthBar.transform.parent.gameObject;
        }

        revalidateProperties = false;
    }

    void Start() {
        spawnPosition = transform.position;
        health = maxHealth;
        hitsCounter = 0;

        /*if (waypoints.Length > 0)
            currentWaypoint = 0;*/

        originalLayer = gameObject.layer;
        UpdateUI();
    }

    public void CanStart() {
        if (gameObject.activeInHierarchy)
            StartCoroutine(CanStartCo());
    }

    IEnumerator CanStartCo() {
        gameObject.transform.GetChild(0).gameObject.SetActive(false);

        yield return new WaitForSeconds(timeBeforeStartSpawn);

        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        currenState = STATE.Moving;
    }

    void Update() {
        if (!LevelManager.IsLevelOnGoing) {
            if (agent.remainingDistance > 0f) {
                agent.SetDestination(transform.position);
                agent.ResetPath();
            }
            if (audioSource.isPlaying)
                audioSource.Stop();
            //currenState = STATE.Destroyed;
            return;
        }

        MiniStateMachine();
    }

    void MiniStateMachine() {
        if (currenState == STATE.Stopped) {
            Stopped();
        } else if (currenState == STATE.Moving) {
            Moving();
        } else if (currenState == STATE.Breaking) {
            Breaking();
        } else if (currenState == STATE.Cooldown) {
            if (cooldownCo == null)
                cooldownCo = StartCoroutine(CooldownCo());
        } else if (currenState == STATE.Destroyed) {
            Destroyed();
        }
    }

    void Stopped() {
        agent.isStopped = true;
        audioSource.Stop();
    }

    void Moving() {
        gameObject.layer = originalLayer;
        agent.isStopped = false;

        if (currentWaypoint == 0) {
            agent.SetDestination(waypoints[currentWaypoint].position);
        }

        if (!audioSource.isPlaying)
            audioSource.Play();

        if (agent.remainingDistance <= agent.stoppingDistance) {
            if (currentWaypoint <= waypoints.Length - 1 && !onDestination) {
                onDestination = true;   // To avoid the battering ram to several waypoints in the same frame.
                currentWaypoint++;

                if (currentWaypoint <= waypoints.Length - 1)
                    agent.SetDestination(waypoints[currentWaypoint].position);
                else {
                    currenState = STATE.Breaking;
                    agent.ResetPath();
                    currentWaypoint--;
                    //Debug.Log($"BatteringRam: {gameObject.name}: CurrentWaypoint: {currentWaypoint} > Waypoints.Length - 1: {waypoints.Length - 1}");
                }
            }
        }
        else {
            onDestination = false;
        }

        /*if (agent.remainingDistance >= agent.stoppingDistance) {
            if (currentWaypoint >= waypoints.Length - 1) {
                currenState = STATE.Breaking;
                agent.ResetPath();
                Debug.Log($"BatteringRam: {gameObject.name}: CurrentWaypoint: {currentWaypoint} >= Waypoints.Length - 1: {waypoints.Length - 1}");
            }
        }*/

        /*Debug.Log($"BatteringRam: {gameObject.name}: \n " +
            $"Remaining distance: {agent.remainingDistance} <= StoppingDistance {agent.stoppingDistance}\n" +
            $"CurrentWaypoint: {currentWaypoint} <= Waypoints.Length - 1: {waypoints.Length - 1}");*/
    }

    void Breaking() {
        agent.isStopped = false;

        if (permanentHitsCounter >= Gate.maxHealthStatic) {
            currenState = STATE.Destroyed;
            return;
        }

        if (hitsCounter != hitsBeforeCooldown) {
            if (timeCounter <= 0) {
                timeCounter = timeBetweenHits;
                //HitDoor();
                // Move forward to hit the door.
                agent.updateRotation = true;
                agent.SetDestination(waypoints[currentWaypoint].position);
                onStepBack = false;
                if (!audioSource.isPlaying)
                    audioSource.Play();
            }
            else {
                timeCounter -= Time.deltaTime;
                // Step back to deliver the next blow.
                if (!onStepBack && agent.remainingDistance <= agent.stoppingDistance) {
                    //agent.SetDestination(transform.position - transform.forward * stepBackDistance);
                    agent.updateRotation = false;
                    agent.SetDestination(transform.position - transform.forward * stepBackDistance);
                    onStepBack = true;
                }
                else if (agent.remainingDistance >= agent.stoppingDistance) {
                    if (audioSource.isPlaying)
                        audioSource.Stop();
                }
            }
        }

        /*if (isEnemyRam)
            Debug.Log($"BatteringRam: {gameObject.name}:  TimeCounter: {timeCounter}");*/

        if (hitsCounter >= hitsBeforeCooldown) {
            currenState = STATE.Cooldown;
            onStepBack = false;
            agent.updateRotation = true;
        }
    }

    IEnumerator CooldownCo() {
        audioSource.Stop();

        gameObject.layer = inactiveLayerIndex;
        agent.isStopped = true;
        gameObject.transform.GetChild(0).gameObject.SetActive(false);
        healthGroupGO.SetActive(false);
        PlayDestroySound();
        transform.position = spawnPosition;

        // ToDo: Waiting x time to be able to respawn again.
        yield return new WaitForSeconds(cooldownTime);

        if (!LevelManager.IsLevelOnGoing)
            StopCoroutine(cooldownCo);

        currentWaypoint = 0;
        health = maxHealth;
        isDead = false;
        hitsCounter = 0;
        timeCounter = 0;
        UpdateUI();
        healthGroupGO.SetActive(true);
        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        agent.ResetPath();
        agent.isStopped = false;
        currenState = STATE.Moving;
        cooldownCo = null;
        //agent.SetDestination(waypoints[0].position);
    }

    void Destroyed() {
        audioSource.Stop();
        PlayDestroySound();
        healthGroupGO.SetActive(false);

        Destroy(gameObject);
    }

    public void TakeDamage(float damage) {
        if (health > 0) {
            health -= damage;
            if (health <= 0) {
                health = 0;
                Die();
            }
            UpdateUI();
        }
    }

    void Die() {
        audioSource.Stop();
        PlayDestroySound();

        isDead = true;
        agent.updateRotation = true;
        currenState = STATE.Cooldown;
    }

    void UpdateUI() {
        healthBar.fillAmount = health / maxHealth;
        healthText.text = $"{health} / {maxHealth}";
    }

    public void HitDoor(int health) {
        hitsCounter++;

        // Based on Gate.maxHealthStatic and current health of the gate, calculate the permanent hits counter from 0 to maxHealthStatic.
        permanentHitsCounter = Gate.maxHealthStatic - health;

        //permanentHitsCounter++;
        //Debug.Log($"BatteringRam: {gameObject.name}: Hitted {hitsCounter} times.");
    }

    /*private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Gate")) {
            HitDoor();
        }
    }*/

    void PlayDestroySound() {
        /*AudioSource sound = new GameObject("tempBatteringRamSound").AddComponent<AudioSource>();
        sound.transform.position = transform.position;
        sound.PlayOneShot(destroySound);*/
        AudioSource sound = Instantiate(destroySound, transform.position, Quaternion.identity);
        Destroy(sound.gameObject, sound.clip.length + 0.01f);


        if (destroyFX != null) {
            /*ParticleSystem fx = */Instantiate(destroyFX, transform.position, Quaternion.identity);
        }
    }
}