using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class TowerColliderProxy : MonoBehaviour {

	[field: SerializeField, GetComponent, ReadOnlyField] private Rigidbody rb { get; set; }
	[field: SerializeField, GetComponent, ReadOnlyField] public DisolveScript disolveScript { get; private set; }
	[field: SerializeField] private Tower tower { get; set; }
	[field: SerializeField] private RotationConstraint rotConstraint { get; set; }
	[field: SerializeField] private PositionConstraint posConstraint { get; set; }

	public void TakeDamage(float damage) {
        tower.TakeDamage(damage);
    }

	public void ApplyExplosionForce() {

		if (rotConstraint != null && posConstraint != null) {
            rotConstraint.constraintActive = false;
			posConstraint.constraintActive = false;
			//Debug.Log("Constraints disabled");
		}

		rb.isKinematic = false;
		// Random explosion direction
		rb.AddExplosionForce(1000f, gameObject.transform.position, 100f, Random.Range(-5f, 5f), ForceMode.Impulse);

		//Debug.Log($"Piece {gameObject.name} has been called");
	}
}