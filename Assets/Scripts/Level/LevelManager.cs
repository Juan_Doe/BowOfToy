using Cinemachine;
using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour {

    // Static Properties
    public static bool IsLevelOnGoing { get; private set; }


    [field: Header("Autoattach On Editor Properties")]
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private PlayerManager playerManager { get; set; }
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private RespawnManager respawnManager { get; set; }
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private EnemiesManager enemiesManager { get; set; }
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private AlliesManager alliesManager { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private AudioSource audioSource { get; set; }

    [field: Header("Level UI Properties")]
    [field: SerializeField] public GameObject hud { get; set; }
    [field: SerializeField] private GameObject endLevelUI { get; set; }
    [field: SerializeField] private float backToMainMenuTime { get; set; } = 15f;
    [field: SerializeField] private Image winImage { get; set; }
    [field: SerializeField] private Image defeatImage { get; set; }
    [field: SerializeField] private Text _legalText { get; set; }
    [field: SerializeField] private string credits { get; set; }

    [field: Header("Level Audio Properties")]
    [field: SerializeField] public AudioMixer audioMixer { get; private set; }

    [field: Header("End Level")]
    [field: SerializeField] private Animator enemyAnimator { get; set; }
    [field: SerializeField] private Animator allyAnimator { get; set; }
    [field: SerializeField] private ParticleSystem enemyParticles { get; set; }
    [field: SerializeField] private ParticleSystem allyParticles { get; set; }
    [field: SerializeField] private CinemachineVirtualCamera playerDeathVirtualCamera { get; set; }
    [field: SerializeField] private Ghost deathPlayer { get; set; }
    [field: SerializeField] private UnityEvent onLevelEnd { get; set; }

    [field: Header("Debug")]
    [field: SerializeField, ReadOnlyField] private bool isLevelOnGoingDebug { get; set; }

    private bool victory { get; set; }
    private bool defeat { get; set; }

    void Awake() {
        IsLevelOnGoing = false;
        isLevelOnGoingDebug = false;
    }

    void Start() {
        audioMixer = audioSource.outputAudioMixerGroup.audioMixer;
        _legalText.text += $" Bow of Toy - Version: {Application.version} \n" +
            $"Credits: {credits}";
        //StartLevel();
    }

    void Update() {
        isLevelOnGoingDebug = IsLevelOnGoing;

        RespawnEnemies();
        RespawnAllies();

        if (!IsLevelOnGoing) {
            if (enemyAnimator != null) {
                if (enemyAnimator.GetCurrentAnimatorStateInfo(0).IsTag("AnimationEndedVictory")) {
                    victory = true;
                    enemyAnimator.gameObject.SetActive(false);
                    enemyParticles.transform.position = enemyAnimator.transform.position;
                    enemyParticles.gameObject.SetActive(true);
                }
            }
                    
            if (allyAnimator != null) {
                if (allyAnimator.GetCurrentAnimatorStateInfo(0).IsTag("AnimationEndedDefeat")) {
                    defeat = true;
                    allyAnimator.gameObject.SetActive(false);
                    allyParticles.transform.position = allyAnimator.transform.position;
                    allyParticles.gameObject.SetActive(true);
                }
            }
            
            if (!victory && !defeat) {
                if (playerDeathVirtualCamera != null) {
                    if (playerManager.Lives <= 0 && !playerDeathVirtualCamera.gameObject.activeInHierarchy) {
                        //Ghost deathP = Instantiate(deathPlayer, Vector3.zero, Quaternion.identity);
                        //playerDeathVirtualCamera.LookAt = deathP.transform;
                        deathPlayer.gameObject.SetActive(true);
                        playerDeathVirtualCamera.gameObject.SetActive(true);
                        StartCoroutine(EndByPlayerDeathCo());
                    }
                }
            }

            if (victory) {
                if (winImage != null)
                    if (winImage.color.a < 1f)
                        winImage.color = new Color(winImage.color.r, winImage.color.g, winImage.color.b, winImage.color.a + Time.deltaTime / 2f);
            }

            if (defeat) {
                if (defeatImage != null)
                    if (defeatImage.color.a < 1f)
                        defeatImage.color = new Color(defeatImage.color.r, defeatImage.color.g, defeatImage.color.b, defeatImage.color.a + Time.deltaTime / 2f);
            }
        }
    }

    public void StartLevel() {
        IsLevelOnGoing = true;
        audioSource.Play();
    }

    public void EndLevel(bool win=true) {
        onLevelEnd.Invoke();

        IsLevelOnGoing = false;
        hud.SetActive(false);
        endLevelUI.SetActive(true);

        if (win) {
            endLevelUI.transform.GetChild(1).gameObject.SetActive(true);
        } else {
            endLevelUI.transform.GetChild(2).gameObject.SetActive(true);
        }

        StartCoroutine(BackToMainMenuCo());
    }

    void RespawnEnemies() {
        if (!IsLevelOnGoing || !respawnManager.respawnEnabled)
            return;

        if (respawnManager.EnemyListCount > 0) {
            respawnManager.GetEnemyFromPool();
        }
    }

    void RespawnAllies() {
        if (!IsLevelOnGoing || !respawnManager.respawnEnabled)
            return;

        if (respawnManager.AllyListCount > 0) {
            respawnManager.GetAllyFromPool();
        }
    }

    IEnumerator BackToMainMenuCo() {
        yield return new WaitForSeconds(backToMainMenuTime);
        BackToMainMenu();
    }

    public void BackToMainMenu() {
        Time.timeScale = 1f;
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void EnableDisableCursor(bool locked) {
        if (PauseManager.onPause)
            locked = false;

        Cursor.lockState = locked ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !locked;
    }

    IEnumerator EndByPlayerDeathCo() {
        yield return new WaitForSeconds(backToMainMenuTime / 2f);
        defeat = true;
    }
}