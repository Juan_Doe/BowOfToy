using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class Arrow : MonoBehaviour {

    [field: Header("Autoattach properties")]
    [field: SerializeField, GetComponent, ReadOnlyField] public Rigidbody rb { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private AudioSource audioSource { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private ScaleConstraint constraint { get; set; }

    [field: Header("Arrow settings")]
    [field: SerializeField] private BoxCollider childColl { get; set; }
    [field: SerializeField] public float damage { get; set; } = 10f;
    [field: SerializeField, ReadOnlyField] private bool stucked { get; set; }

    [field: Header("Arrow sounds")]
    [field: SerializeField] private AudioClip flySound { get; set; }
    [field: SerializeField] private AudioClip hitSound { get; set; }

    private void OnTriggerEnter(Collider other) {
        if (stucked) return;

        if (other.CompareTag("Untagged")) {
            StickOnPoint(other.transform);
        }

        // Compare if the target is an enemy and the arrow is an ally.
        if (other.CompareTag("Enemy") && gameObject.layer == 10) {
            StickOnPoint(other.transform);
            other.GetComponent<EnemyArcher>().TakeDamage(damage);
            return;
        }

        // Compare if the target is a Ghost and the arrow is an ally.
        if (other.CompareTag("Ghost") && gameObject.layer == 10) {
            StickOnPoint(other.transform);
            other.GetComponent<Ghost>().TakeDamage(damage);
            return;
        }

        // Compare if the target is a Tower and the arrow is an ally.
        if (other.CompareTag("Tower") && gameObject.layer == 10) {
            StickOnPoint(other.transform);
            other.GetComponent<TowerColliderProxy>().TakeDamage(damage);
            return;
        }

        // Compare if the target is a Battering Ram and the arrow is an ally.
        if (other.CompareTag("BatteringRam") && gameObject.layer == 10) {
            StickOnPoint(other.transform);
            other.GetComponent<BatteringRamColliderProxy>().TakeDamage(damage);
            return;
        }

        // Compare if the target is a Battering Ram and the arrow is an enemy.
        if (other.CompareTag("BatteringRam") && gameObject.layer == 11) {
            StickOnPoint(other.transform);
            other.GetComponent<BatteringRamColliderProxy>().TakeDamage(damage);
            return;
        }

        // Compare if the taget is a Tower and the arrow is an enemy.
        if (other.CompareTag("Tower") && gameObject.layer == 11) {
            StickOnPoint(other.transform);
            other.GetComponent<TowerColliderProxy>().TakeDamage(damage);
            return;
        }

        // Compare if the target is an ally and the arrow is an enemy.
        if (other.CompareTag("Ally") && gameObject.layer == 11) {
            StickOnPoint(other.transform);
            other.GetComponent<AllyArcher>().TakeDamage(damage);
            return;
        }

        if (other.CompareTag("Player") && gameObject.layer == 11) {
            StickOnPoint(other.transform);
            other.GetComponent<PlayerManager>().TakeDamage(damage);
            return;
        }

        if ((other.CompareTag("PlayerBase") && gameObject.layer == 11) || 
            (other.CompareTag("EnemyBase") && gameObject.layer == 10)) {

            StickOnPoint(other.transform);
            other.GetComponent<BaseHealthAlpha>().TakeDamage(damage);
            return;
        }
    }

    private void StickOnPoint(Transform parent) {
        stucked = true;
        rb.isKinematic = true;
        rb.useGravity = false;

        if (constraint != null) {
            constraint.AddSource(new ConstraintSource {
                sourceTransform = parent,
                weight = 1
            });
            constraint.constraintActive = true;
        }

        transform.SetParent(parent);
        //FixScale();

        if (hitSound != null)
            audioSource.PlayOneShot(hitSound);
    }

    void FixScale() {
        Vector3 scaleFixed = new Vector3(
            transform.localScale.x / transform.parent.localScale.x,
            transform.localScale.y / transform.parent.localScale.y,
            transform.localScale.z / transform.parent.localScale.z);

        transform.localScale = scaleFixed;
    }

    public void PlayFlySound() {
        if (audioSource != null) {
            audioSource.PlayOneShot(flySound);
        }
    }
}