using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class Gate : MonoBehaviour {

    [field: Header("Autoattach properties")]
    [field: SerializeField, GetComponent, ReadOnlyField] private AudioSource audioSource { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private DisolveScript dissolveScript { get; set; }

    [field: Header("Gate properties")]
    public static int maxHealthStatic { get; private set; } = 6;
    [field: SerializeField] private int maxHealth { get; set; } = 6;
    [field: SerializeField, ReadOnlyField] public int health { get; set; }
    [field: SerializeField] private float damageCooldown { get; set; } = 8f;

    [field: Header("UI properties")]
    [field: SerializeField] private Image healthBar { get; set; }
    [field: SerializeField, ReadOnlyField] private Text healthText { get; set; }
    [field: SerializeField] private bool revalidateProperties { get; set; }

    [field: Header("Gate sounds")]
    [field: SerializeField] private AudioClip hitSound { get; set; }
    [field: SerializeField] private AudioClip destroySound { get; set; }

    private bool isDead { get; set; }
	private bool inCooldown { get; set; }

    void OnValidate() {
#if UNITY_EDITOR
        UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
        bool prefabConnected = PrefabUtility.GetPrefabInstanceStatus(this.gameObject) == PrefabInstanceStatus.Connected;
        if (!isValidPrefabStage && prefabConnected) {
            // Variables that will only be checked when they are in a scene
            if (!Application.isPlaying) {
                if (revalidateProperties)
                    ValidateAssings();
            }
        }
#endif
    }

    void ValidateAssings() {
        if (healthText == null || revalidateProperties) {
            healthText = healthBar.transform.GetChild(0).GetComponent<Text>();
        }

        revalidateProperties = false;
    }

    void Start() {
        maxHealthStatic = maxHealth;
        health = maxHealth;
        UpdateUI();
    }

    public int TakeDamage(int damage) {
        if (health > 0) {
            
            health -= damage;
            audioSource.PlayOneShot(hitSound);

            if (health <= 0) {
                health = 0;
                Die();
            }
            UpdateUI();
            if (!isDead)
                StartCoroutine(DamageCooldownCo());
        }
        return health;
    }

    void Die() {
        isDead = true;

        AudioSource sound = new GameObject("tempGateSound").AddComponent<AudioSource>();
        sound.PlayOneShot(destroySound);
        Destroy(sound.gameObject, destroySound.length + 0.01f);

        StartCoroutine(DisableEffectCo());
    }

    void UpdateUI() {
        healthBar.fillAmount = (float) health / (float) maxHealth;  // (float) is necessary to avoid fillamount directly to 0
        healthText.text = $"{health} / {maxHealth}";
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("BatteringRam")) {
            if (!inCooldown) {
                collision.gameObject.GetComponent<BatteringRam>().HitDoor(TakeDamage(1));
            }
        }
    }

    IEnumerator DamageCooldownCo() {
        inCooldown = true;
        yield return new WaitForSeconds(damageCooldown);
        inCooldown = false;
    }

    IEnumerator DisableEffectCo() {
        dissolveScript.Dissolve();

        while (!dissolveScript.dissolved) {
            yield return new WaitForSeconds(0.1f);
        }

        gameObject.SetActive(false);
    }
}